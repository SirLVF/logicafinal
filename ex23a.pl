agente(aviao,[voa]).
agente(planador,[tipo_motor(nenhum)]).
agente(motorizado,[tipo_motor(helice)]).
agente(monomotor,[numero_motores(1)]).
agente(bimotor,[numero_motores(2)]).
agente(jacto,[tipo_motor(turbina)]).
agente(supersonico(V),[velocidade(V)]).
isa(planador,aviao).
isa(motorizado,aviao).
isa(monomotor,motorizado).
isa(bimotor,motorizado).
isa(jacto,aviao).
isa(supersonico(_),jacto).


