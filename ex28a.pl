:-[ex28b],
	dynamic(stock/2), dynamic(maxtime/1).

perfume([rosas:1,jasmim:1,alcool:5,ambar:1,tempo:6],romance).
perfume([bergamota:1,acool:3,balsamo:1,tempo:12],clinique-citrus).
perfume([baunilha:1,rosas:2,alcool:4,almiscarado:1,tempo:18],cacharel-amor).
stock(alcool,20).
stock(jasmim,2).
stock(baunilha,2).
stock(bergamota,1).
stock(rosas,2).
stock(ambar,1).
stock(balsamo,1).
stock(almiscarado,1).
maxtime(0).
[perfume(L,P),emstock(L),member(tempo:T,L),maxtime(M)]--->[ write(P),nl,removestock(L),substime(M,T)].
[maxtime(X)]--->[write(X),write(' meses. '),nl,stop].
emstock([]).
emstock([tempo:_|R]):-emstock(R).
emstock([X:N1|R]):-stock(X,NS),N1=<NS,emstock(R).
removestock([]).
removestock([tempo:_|R]):-removestock(R).
removestock([X:N1|R]):-	stock(X,NS),
						N is NS-N1,
						substitui( stock(X,NS), stock(X,N) ),
						removestock(R).
substime(M,T):- M=<T, substitui(maxtime(M),maxtime(T)).
substime(_,_).
