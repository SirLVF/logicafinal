:-op(800,xfx,--->).
:-op(600,fx,~).
demo:- condicao--->accao,
	testa(condicao),
	executa(accao).
testa([]).
testa([~Primeira|Resto]):-
!,nao(Primeira),testa(Resto).
testa([Primeira|Resto]):-
!,call(Primeira),testa(Resto).
nao(condicao):-call(condicao),!,fail.
nao(_).
executa([stop]):-!.
executa([]):- demo.
executa([Primeira|Resto])
:-call(Primeira), executa(Resto).
substitui(A,B):- retract(A),!,asserta(B).
insere(A):- assrtea(A).
retira(A):- retract(A).
