:-[ex28b],dynamic(jogo/4),dynamic(pontos/2).

jogo(portugal,espanha,2,1).
jogo(alemanha,portugal,1,1).
jogo(portugal,grecia,2,0).
jogo(alemanha,grecia,1,1).
jogo(alemanha,espanha,1,1).
jogo(espanha,grecia,1,0).

pontos(portugal,0).
pontos(espanha,0).
pontos(alemanha,0).
pontos(grecia,0).

[jogo(E1,E2,G1,G2),pontos(E1,P1),pontos(E2,P2)]--->
	[pontosjogo(G1,G2,PJ1,PJ2),
	PN1 is P1+PJ1,PN2 is P2+PJ2,
	substitui(pontos(E1,P1),pontos(E1,PN1)),
	substitui(pontos(E2,P2),pontos(E2,PN2)),
	retira(jogo(E1,E2,G1,G2))].
[findall(P,pontos(_,P),L),maximo(PM,L),pontos(E,PM)]--->
	[write(pontos(E,PM)),nl,retira(pontos(E,PM))].
[]--->[stop].

pontosjogo(G1,G1,1,1).
pontosjogo(G1,G2,3,0):-G1>G2.
pontosjogo(_,_,0,3).

maximo(X,Y,X):- X>Y.
maximo(_,Y,Y).

maximo(M,[M]).
maximo(M,[P|R]):- maximo(M1,R),maximo(P,M1,M).
